summary: Verifies that max kernel stack size is less than 16k
description: |
    This script validates the kernel maximum stack size using tracer.
    It should be run at the end of a test round but without the machine being rebooted, because
    rebooting turns off the tracer.

    The test works as follows:
        Verifies that max kernel stack size is less than 16k

    First it checks if the stack tracer is enabled and logs a warning then exits if not.

    If enabled it checks the current stack_max_size value, and compares it against a predefined threshold.

    If the stack_max_size exceeds the threshold, an error is logged, and the
    function responsible for the maximum stack usage is identified.

    Inputs:
        THRESHOLD=13312
        /sys/kernel/tracing/stack_max_size
        /sys/kernel/tracing/stack_trace
        /proc/sys/kernel/stack_tracer_enabled
        current_value=$(cat /sys/kernel/tracing/stack_max_size)
        compare $current_value to $THRESHOLD

    Expected results:
        [   PASS   ] :: File '$TMT_PLAN_DATA/stack_results.log' should not contain 'RESULTS: WARN'
        [   PASS   ] :: File '$TMT_PLAN_DATA/stack_results.log' should not contain 'RESULTS: FAIL'
        [   LOG    ] :: Stack trace was enabled and no test failures.
        [   PASS   ] :: File '/proc/sys/kernel/stack_tracer_enabled' should contain '1'
        [   PASS   ] :: Check if the current value exceeds the threshold (Assert: "$current_value" should be lesser than "$THRESHOLD")

    Results location:
        output.txt.
contact: mm-qe <mm-qe@redhat.com>
test: bash ./runtest.sh
framework: beakerlib
id: 519bce43-f03c-4345-bad4-b8ec9382257a
duration: 15m
extra-summary: validate_kernel_stack_max_size
extra-task: validate_kernel_stack_max_size
